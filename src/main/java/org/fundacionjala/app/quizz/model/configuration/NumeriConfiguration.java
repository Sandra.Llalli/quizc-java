package org.fundacionjala.app.quizz.model.configuration;

import org.fundacionjala.app.quizz.model.validator.ValidatorType;

public class NumeriConfiguration extends QuestionConfiguration{
	 public NumeriConfiguration() {
	        super(false, ValidatorType.REQUIRED, ValidatorType.MAX);
	    }
}
