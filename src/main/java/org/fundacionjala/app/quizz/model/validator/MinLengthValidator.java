package org.fundacionjala.app.quizz.model.validator;

import java.util.List;

public class MinLengthValidator implements Validator {

    private static final String ERROR_MESSAGE = "The value length must be less than ";
    private static final String ERROR_MESSAGE_MAX_LENGTH = "The value length must be less than ";
    private static final String ERROR_MESSAGE_CAPITAL_TEXT = "The value length must be less than ";

    @Override
    public void validate(String value, String conditionValueString, List<String> errors) {
        try {
            int conditionValue = IntegerParser.parse(conditionValueString);

            if (value.length() < conditionValue) {
                errors.add(ERROR_MESSAGE + conditionValue);
            }
            if (value.length() > conditionValue) {
            	errors.add(ERROR_MESSAGE_MAX_LENGTH + conditionValue);
            }
          
            if(value.equals(value.toUpperCase())) {
        	   errors.add(ERROR_MESSAGE_CAPITAL_TEXT + conditionValue);
           }
        } catch (NumberFormatException exception) {
            exception.printStackTrace();
            errors.add(IntegerParser.ERROR_MESSAGE_INVALID_NUMBER);
        }

    }
}
