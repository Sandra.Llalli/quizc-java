package org.fundacionjala.app;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

import org.fundacionjala.app.quizz.model.Question;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.stream.JsonReader;

public class Json {
	public Json(Question question) {
		writeJsonFile(question);
	}

    private static Question readJsonFile() {
        Gson gson = new Gson();
        Question question = null;
        try (JsonReader reader = new JsonReader(new FileReader("./myForm.json"))) {
        	question = gson.fromJson(reader, Question.class);
        } catch (IOException exception) {
            exception.printStackTrace();
        }

        return question;
    }

    public static void writeJsonFile(Question question) {
        Gson gson = new Gson();
        try (Writer writer = new FileWriter("./myForm.json")) {
            gson.toJson(question, writer);
        } catch (JsonIOException | IOException exception) {
            exception.printStackTrace();
        }
    }
}
